package golib

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"
)

func SliceToSqlStr(array interface{}) string {
	var params []string
	switch array.(type) {
	case []string:
		params = array.([]string)
	case []int:
		tmp := array.([]int)
		params = make([]string, 0, len(tmp))
		for _, val := range tmp {
			params = append(params, fmt.Sprintf("%v", val))
		}
	case []int64:
		tmp := array.([]int64)
		params = make([]string, 0, len(tmp))
		for _, val := range tmp {
			params = append(params, fmt.Sprintf("%v", val))
		}
	default:
		log.Println("SliceToSqlStr unsupport type.")
		return ""
	}

	return "{" + strings.Join(params, ", ") + "}"
}

type PostgresArray []uint8

func (pa PostgresArray) String() string {
	return string(pa)
}

func (pa PostgresArray) GetStringArray() (str_arr []string) {
	str := strings.TrimLeft(string(pa), "{")
	str = strings.TrimRight(str, "}")
	old_str_arr := strings.Split(str, ",")
	str_arr = make([]string, 0, len(old_str_arr))
	for _, s := range old_str_arr {
		s = strings.Trim(strings.TrimSpace(s), `"`)
		if s != "" {
			str_arr = append(str_arr, s)
		}
	}
	return
}

func (pa PostgresArray) GetInt64Array() (int64_arr []int64, err error) {
	str_arr := pa.GetStringArray()
	int64_arr = make([]int64, 0, len(str_arr))
	var i int64
	for _, str := range str_arr {
		if i, err = strconv.ParseInt(str, 10, 64); err != nil {
			return
		}
		int64_arr = append(int64_arr, i)
	}
	return
}

type NullPostgresArray struct {
	PostgresArray PostgresArray
	Valid         bool // Valid is true if SliceInt64 is not NULL
}

func (this *NullPostgresArray) Scan(value interface{}) (err error) {
	if value == nil {
		this.Valid = false
		return
	}
	if si, ok := value.([]uint8); ok {
		this.PostgresArray = PostgresArray(si)
		this.Valid = true
	} else {
		err = errors.New("asset type err.")
		log.Println(value)
	}
	return
}

type NullTime struct {
	Time  time.Time
	Valid bool
	Int64 int64
}

func (this *NullTime) Scan(value interface{}) (err error) {
	if value == nil {
		this.Valid = false
		return
	}
	if t, ok := value.(time.Time); ok {
		this.Time = t
		this.Int64 = t.Unix()
		this.Valid = true
	} else {
		err = errors.New("asset type err.")
		log.Println(value)
	}
	return
}

type SelectFilter map[string]interface{}

func (sf *SelectFilter) GetSQL(args ...map[string]interface{}) (sql_str, page_str string, params []interface{}, err error) {
	const per = 10
	i := 0
	page := 1
	var a time.Time
	nonEmpty := true
	for k, v := range *sf {
		nonEmpty = true
		if k == "page" {
			p, err := strconv.Atoi(v.(string))
			log.Println(p)
			if err != nil {
				page = 1
			} else {
				page = p
			}
			if page < 1 {
				page = 1
			}
			continue
		}
		switch v.(type) {
		case string:
			if v.(string) == "" {
				nonEmpty = false
			}
		case time.Time:
			if v.(time.Time).Unix() == a.Unix() {
				nonEmpty = false
			}
		}
		if nonEmpty {
			i++
			sql_str = sql_str + k + "=$" + strconv.Itoa(i) + " and "
			params = append(params, v)
		}
	}
	//约定规则,实现比较大小,及模糊查询
	for _, arg := range args {
		name := arg["name"].(string)
		if name == "" {
			continue
		}
		ne := arg["ne"] //不等于
		gt := arg["gt"] //大于
		lt := arg["lt"] //小于
		ge := arg["ge"] //大于等于
		le := arg["le"] //小于等于
		like := arg["like"]

		if ne != nil {
			i++
			sql_str = sql_str + name + "<>$" + strconv.Itoa(i) + " and "
			params = append(params, ne)
		}
		if gt != nil {
			i++
			sql_str = sql_str + name + ">$" + strconv.Itoa(i) + " and "
			params = append(params, gt)
		}
		if lt != nil {
			i++
			sql_str = sql_str + name + "<$" + strconv.Itoa(i) + " and "
			params = append(params, lt)
		}
		if ge != nil {
			i++
			sql_str = sql_str + name + ">=$" + strconv.Itoa(i) + " and "
			params = append(params, ge)
		}
		if le != nil {
			i++
			sql_str = sql_str + name + "<=$" + strconv.Itoa(i) + " and "
			params = append(params, le)
		}
		if like != nil {
			i++
			sql_str = sql_str + name + " like '%$" + strconv.Itoa(i) + "%' and "
			params = append(params, like)
		}
	}
	if sql_str == "" {
		sql_str = "1=$1"
		params = []interface{}{"1"}
	} else {
		lStr := len(sql_str) - 5
		sql_str = sql_str[:lStr]
	}
	page_str = fmt.Sprintf(" limit %v offset %v", per, per*(page-1))
	return
}
