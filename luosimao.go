package golib

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

const (
	LSM_URL = "http://sms-api.luosimao.com/v1/send.json"
)

//call luosimao
func CallLSM(phone, message, api_key string) (err error) {
	if !CheckPhone(phone) {
		err = errors.New(fmt.Sprintf("phone err: %v", phone))
		return
	}
	uv := url.Values{"mobile": {phone}, "message": {message}}
	req, err := http.NewRequest("POST", LSM_URL, strings.NewReader(uv.Encode()))
	if err != nil {
		return
	}
	req.SetBasicAuth("api", api_key)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return
	}

	type Ret struct {
		Code int    `json:"error"`
		Msg  string `json:"msg"`
	}
	var ret Ret

	err = json.Unmarshal([]byte(respBody), &ret)
	if err != nil {
		return
	}

	if ret.Code != 0 {
		err = errors.New(fmt.Sprintf("callLSM, ret: %v, msg: %v", ret.Code, ret.Msg))
	}
	return
}
