package golib

import (
	"net"
	"regexp"
)

var (
	//regexp
	reg_phone     = regexp.MustCompile(`^1\d{10}$`)
	reg_person_id = regexp.MustCompile(`^\d{18}|\d{15}$`)
	reg_yacht_id  = regexp.MustCompile(`^.+$`)
	//rule
	LEN_MD5 = 32
)

//CheckPhone
func CheckPhone(phone string) bool {
	return reg_phone.MatchString(phone)
}

//CheckPwd
func CheckMD5(pwd string) bool {
	if len(pwd) == LEN_MD5 {
		return true
	}
	return false
}

//CheckIP
func CheckIP(ip string) bool {
	if ipo := net.ParseIP(ip); ipo != nil {
		return true
	}
	return false
}

//CheckPersonID
func CheckPersonID(person_id string) bool {
	return reg_person_id.MatchString(person_id)
}

//CheckYachtID
func CheckYachtID(yacht_id string) bool {
	return reg_yacht_id.MatchString(yacht_id)
}
