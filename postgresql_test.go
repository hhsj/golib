package golib

import (
	"database/sql"
	"testing"

	_ "github.com/lib/pq"
)

func Test_PQ(t *testing.T) {
	db, err := sql.Open("postgres", "host=localhost user=postgres dbname=postgres sslmode=disable")
	if err != nil {
		t.Fatal(err)
	}
	if err = db.Ping(); err != nil {
		t.Fatal(err)
	}

	// test array
	if _, err = db.Exec("create table test_array(id serial primary key, phone int8[], content character varying[]); "); err != nil {
		t.Fatal(err)
	}

	// test []int
	arr_int := []int{1, 2, 3}
	if _, err = db.Exec("insert into test_array(phone) values($1);", SliceToSqlStr(arr_int)); err != nil {
		t.Error(err)
	}
	var a int
	if row := db.QueryRow("select phone[1] from test_array where id=1"); row != nil {
		if err := row.Scan(&a); err != nil {
			t.Error(err)
		} else if a != arr_int[0] {
			t.Fail()
		}
	} else {
		t.Fail()
	}

	// test []string
	arr_string := []string{"a", "b", "c"}
	if _, err = db.Exec("insert into test_array(content) values($1);", SliceToSqlStr(arr_string)); err != nil {
		t.Error(err)
	}
	var b string
	if row := db.QueryRow("select content[1] from test_array where id=2"); row != nil {
		if err := row.Scan(&b); err != nil {
			t.Error(err)
		} else if b != arr_string[0] {
			t.Fail()
		}
	} else {
		t.Fail()
	}

	// drop
	if _, err = db.Exec("drop table test_array; "); err != nil {
		t.Fatal(err)
	}
	// TODO test jsonb
}
