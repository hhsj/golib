package golib

import (
	"errors"
	"net/http"
	"strings"

	"github.com/revel/revel"
)

//get remote ip
func GetRemoteIP(req interface{}) (realIP string, err error) {
	var hdr http.Header
	var remoteAddr string
	if r, ok := req.(*revel.Request); ok {
		hdr = r.Header
		remoteAddr = r.RemoteAddr
	} else if r2, ok2 := req.(*http.Request); ok2 {
		hdr = r2.Header
		remoteAddr = r2.RemoteAddr
	} else {
		err = errors.New("Not *revel.Request or *http.Request")
		return
	}

	realIP = hdr.Get("X-Real-Ip")
	forwardedFor := hdr.Get("X-Forwarded-For")
	if realIP == "" && forwardedFor == "" {
		realIP = strings.Split(remoteAddr, ":")[0]
	} else if forwardedFor != "" {
		ips := strings.Split(forwardedFor, ",")
		realIP = strings.TrimSpace(ips[len(ips)-1])
	}
	return
}
