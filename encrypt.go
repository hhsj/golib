package golib

import (
	"crypto/md5"
	"encoding/hex"
)

//md5
func MD5(s string) (m string, err error) {
	h := md5.New()
	_, err = h.Write([]byte(s))
	if err != nil {
		return
	}
	m = hex.EncodeToString(h.Sum(nil))
	return
}
